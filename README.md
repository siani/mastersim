# MasterSim #

MasterSim is a Doman Specific Language (DSL) based solution to create master algorithms for the standard of co-simulation [FMI](https://www.fmi-standard.org/).

MasterSim has been developed by [SIANI](http://www.siani.es) and is released under [GPL v3.0](http://www.gnu.org/licenses/gpl-3.0.en.html).

## Get started ##

To use MasterSim, you need to install [Intellij](https://www.jetbrains.com/idea/). Once you have installed, please, install plugins available in the download section of this webpage: Tara and Legio.

To install them, go to the settings/plugins in Intellij. Click on install plugins from disk and select the files downloaded before (one by one).

## Your first MasterSim project ##

Once you have the IDE ready for using MasterSim, you can go ahead with the creation of your own Master Algorithm for FMI. To do so, please create a new project and select the Tara facet.

![Captura de pantalla 2016-10-24 a las 17.48.16.png](https://bitbucket.org/repo/xyM64b/images/1531657500-Captura%20de%20pantalla%202016-10-24%20a%20las%2017.48.16.png)

Click on next twice and write the project name you prefer. In our example, we have chosen "Environment" as the name of the project. Once you click on finish, you'll see a structure of the project like this:

![Captura de pantalla 2016-10-25 a las 18.31.02.png](https://bitbucket.org/repo/oaejj4/images/3547427478-Captura%20de%20pantalla%202016-10-25%20a%20las%2018.31.02.png)

Open the "configuration.legio" file and modify the part of the configuration of the factory to something like this:

```
#!python
	Factory as System
		generationPackage = "org.example"
		Modeling(language = "MasterSim", version = "LATEST")
```

Once you're done with it, reload legio by clicking the button in the left-upper corner. Now, you can start to make your own Master Algorithm. For that, make a package you want in the src folder and create a Tara file.

## Model your simulation graph ##

Now that you have your first Tara file created, be sure to fully use all the advantages of the auto-completion tool included in Tara. By pressing ctrl + space, you'll be able to see all the options you have.

In this tutorial, we will model a very simplistic graph of simulations that we are going to use for co-simulate:

```
#!python

dsl MasterSim

CoSimulation(startTime = 0, stopTime = 10, stepSize = 0.01) cs
	variablesToLog = cs.environment.temperature cs.building.externalTemperature
	SimUnit("Environment.fmu") environment
		Variable temperature as Real Output
	SimUnit("Building.fmu") building
		Variable externalTemperature as Real Input
	Link(environment.temperature, building.externalTemperature, @fahrenheitToCelsius)
```

We hope most of the things are easy to understand. That's one of the strongest points of MasterSim. However, as it's your first read, let me help you a little bit. First we are declaring a CoSimulation. Inside, we are declaring two simulation units providing the fmu file path. To include them (they are available in the downloads section), please create a res folder in your project and mark it as resources folder (right click on the folder: mark as...). Then we are declaring the variables for each of the simulation units and creating a link from the output of the Environment to the input of the Building. Note that there is a conversion stated from Fahrenheit to Celsius.

This operation is marked by the @ symbol. This is the way in which you can insert Java code in your models. Here, we provide you with how those methods are implemented, but we are very sure that after some trainings more, you'll be able to come up with the idea. 

To know where these methods are located, just point the selector in one of the methods that are in red and press alt + enter. This is the way to fix something in Intellij. This way it will suggest you to create the method and will give you until this place so that you can fill it out.

```
#!java
    public static void fahrenheitToCelsius(org.siani.mastersim.Link self) {
        self.input().as(RealVariable.class)
                .write((self.outputs().get(0).as(RealVariable.class).value() - 32) * 5.0 / 9.0);
    }
```

Please, make sure you're compiled the previous model so that all generated classes are available for writing this code. If not, you can do it by pressing control + shift + F9 selecting the module in the project view. If not, make also sure that "gen" folder is marked as generated sources (right click on the folder inside intellij/mark as...). As you can see in this code, we are taking the input variable of the link and writing it with the output variable of the link making the transformation from Fahrenheit to Celsius degrees.

Next step is to create your executable class and include this portion of code:

```
#!java
public static void main(String[] args) {
        Graph.load().wrap(MasterSimApplication.class).application().execute();
    }

```

As result, you'll find a csv file containing the results of the simulation.

## What's next? ##

We recommend you to play using the auto-completion code and to join us in the community opening discussions in the issue tracker. However, we are hardly working on creating a documentation to let you know what else you can do with MasterSim.